import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-sis8300drv-ess-linux')


def test_kernel_module_present(host):
    # module should automatically be loaded by udev
    # if the required PCI card is present
    # it's not loaded in the VM as there is no PCI card
    # we just check it's present
    cmd = host.run("/sbin/modinfo sis8300drv")
    assert "sis8300drv.ko" in cmd.stdout
